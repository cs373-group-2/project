from init import app

# start app host='0.0.0.0', port=5050
if __name__ == "__main__":
   app.run()
